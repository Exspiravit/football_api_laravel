<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    //
    protected $fillable = [
        'name', 'position', 'goals',
    ];

    // aqui obtenemos todos los contratos que a tenido el jugador anteriormente, con los clubes en los que jugo
    public function contracts()
    {
        return $this->belongsToMany('App\Models\Club', 'contracts', 'player_id', 'club_id');
    }
}
